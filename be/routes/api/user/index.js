var express = require('express');
var createError = require('http-errors');
var router = express.Router();

const User = require('../../../models/users')

router.get('/', function(req, res, next) {
  console.log(req.query);
  User.find()
    .then(r => {
      res.send({ success:true, users: r});
    })
    .catch(e => {
      console.log(e);
      res.send({ success: false}); 
    })
});

router.post('/', (req, res, nest) => {
  console.log(req.body)
  const { name, age } = req.body
  const u = new User({
    name: name,
    age: age
  })
  u.save()
    .then(r => {
      res.send({ success:true, msg:'post' })
    })
    .catch(e => {
      console.log(e);
      res.send({ success: false}); 
    })
})

router.put('/:id', (req, res, nest) => {
  const id = req.params.id
  const { name, age } = req.body
  User.updateOne({ _id: id }, { $set: { name, age}})
    .then(r => {
      res.send({ success:true, msg: r })
    })
    .catch(e => {
      res.send({ success: false, msg: e.message })
    })
})

router.delete('/:id', (req, res, nest) => {
  const id = req.params.id
  User.deleteOne({ _id: id })
    .then(r => {
      res.send({ success: true, msg: r })
    })
    .catch(e => {
      res.send({ success: false, msg: e.message})
    })
})


module.exports = router;
