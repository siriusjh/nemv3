var express = require('express');
var createError = require('http-errors');
var router = express.Router();

//middle
router.all('*', function(req, res, next) {
  console.log(req.headers)
  if (req.path === '/xxx') return res.send({ status: 'ok' })
  
  next()
})


router.get('/hello', function(req, res, next) {
  res.send({ title: 'api', message: 'hello' });
});


router.use('/test', require('./test'));
router.use('/user', require('./user'));

router.all('*', function(req, res, next) {
  next(createError(404, 'no api'));
});



module.exports = router;
