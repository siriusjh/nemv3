var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const history = require('connect-history-api-fallback');
const cors = require('cors');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());
app.use('/api', require('./routes/api'));
app.use(history());

app.use(express.static(path.join(__dirname, '../', 'fe', 'dist')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({msg: err.message});
});

module.exports = app;


const cfg = require('../config')

const mongoose = require('mongoose');
const User = require('./models/users')

mongoose
  .connect(cfg.dbUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true 
  })
  .then(() => {
    console.log('connected to mongoDb')
  })
  .catch((err) => {
    console.log(err);
  })



//  User.create({ name: '지현'})
//   .then(r => console.log(r))
//   .catch(e => console.error(e))

//  User.find()
//    .then(r => console.log(r))
//    .catch(e => console.error(e))

// User.updateOne({_id: '60dbc8e86481391cf83103c1'}, { $set: { age : 33} })
//   .then(r => {
//     console.log(r)
//     console.log('updated')
//     return User.find();
//   })
//   .then(r => console.log(r))
//   .catch(e => console.error(e))

// User.deleteOne({ name: '지현'})
//   .then(r => console.log(r))
//   .catch(e => console.error(e))


var jwt = require('jsonwebtoken');
const key = 'key'
var token = jwt.sign({ id: 'bar', email: 'test@gmail.com'}, key);
console.log(token)
 
var decoded = jwt.verify(token, key)
console.log(decoded)
console.log(new Date(decoded.iat * 1000))